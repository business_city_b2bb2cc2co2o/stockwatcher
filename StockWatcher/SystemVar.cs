﻿using StockWatcher.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWatcher
{
    class SystemVar
    {
        //文章地址:http://blog.sina.com.cn/s/blog_58fc3aad01015nu7.html

        public readonly static string PRICE_URL = @"http://hq.sinajs.cn/list=";

        public readonly static string PRICE_IMG_URL = @"http://image.sinajs.cn/newchart/min/n/{0}.gif";

        public static FrmNote noteForm;

        public static Dictionary<String,StockConfig> StockConfigs = new Dictionary<String, StockConfig>();

    }
}
