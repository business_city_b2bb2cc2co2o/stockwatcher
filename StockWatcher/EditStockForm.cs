﻿using StockWatcher.Facade;
using StockWatcher.Model;
using StockWatcher.Model.RuleConfigs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StockWatcher
{
    public partial class EditStockForm : Form
    {
        public StockConfig StockInfo { get; set; }

        public EditStockForm()
        {
            InitializeComponent();
            StockInfo = new StockConfig();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnGetStock_Click(object sender, EventArgs e)
        {
            try
            {
                StockInfo.Code = txtCode.Text.Trim();
                StockInfo.RealCode = Facade.StockHelper.GetRealStockCode(StockInfo.Code);
                if (SystemVar.StockConfigs.ContainsKey(StockInfo.RealCode))
                    throw new Exception("这支股票已经添加过了");
                var testStockInfo = SinaDataSource.ReadData(new List<string>() { StockInfo.RealCode });
                if (string.IsNullOrEmpty(testStockInfo[0].股票名字))
                    throw new Exception("股票代码不存在");

                StockInfo.Name = testStockInfo[0].股票名字;
                lbName.Text = StockInfo.Name;
                btnSave.Enabled = true;
                numPriceCost.Value = Convert.ToDecimal(testStockInfo[0].当前价格);
                numRateAbove.Value = Convert.ToDecimal(testStockInfo[0].涨幅 + 2) < numRateAbove.Maximum ? Convert.ToDecimal(testStockInfo[0].涨幅 + 2) : numRateAbove.Maximum;
                numRateBelow.Value = Convert.ToDecimal(testStockInfo[0].涨幅 - 2) > numRateAbove.Minimum ? Convert.ToDecimal(testStockInfo[0].涨幅 - 2) : numRateAbove.Minimum;
                numPriceAbove.Value = Convert.ToDecimal(testStockInfo[0].当前价格);
                numPriceBelow.Value = Convert.ToDecimal(testStockInfo[0].当前价格);
            }
            catch (Exception ex)
            {
                btnSave.Enabled = false;
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            StockInfo.PriceCost = numPriceCost.Value;
            StockInfo.Enable = cbEnable.Checked;
            if (cbPriceAbove.Checked)
            {
                StockInfo.Rule_PriceAbove.Enable = true;
                StockInfo.Rule_PriceAbove.AlertPrice = numPriceAbove.Value;
            }
            else
                StockInfo.Rule_PriceAbove.Enable = false;

            if (cbPriceBelow.Checked)
            {
                StockInfo.Rule_PriceBelow.Enable = true;
                StockInfo.Rule_PriceBelow.AlertPrice = numPriceBelow.Value;
            }else
                StockInfo.Rule_PriceBelow.Enable = false;

            if (cbAmountChange.Checked)
                StockInfo.Rule_AmountChange.Enable = true;
            else
                StockInfo.Rule_AmountChange.Enable = false;

            if (cbPriceChange.Checked)
            {
                StockInfo.Rule_PriceChange.Enable = true;
                StockInfo.Rule_PriceChange.ChangeRatePercent = numPriceChange.Value;
            }
            else
                StockInfo.Rule_PriceChange.Enable = false;

            if (cbRateAbove.Checked)
            {
                StockInfo.Rule_RateAbove.Enable = true;
                StockInfo.Rule_RateAbove.AlertRate = numRateAbove.Value;
            }
            else
                StockInfo.Rule_RateAbove.Enable = false;

            if (cbRateBelow.Checked)
            {
                StockInfo.Rule_RateBelow.Enable = true;
                StockInfo.Rule_RateBelow.AlertRate = numRateBelow.Value;
            }
            else
                StockInfo.Rule_RateBelow.Enable = false;

            if (cbPeek.Checked)
                StockInfo.Rule_Peek.Enable = true;
            else
                StockInfo.Rule_Peek.Enable = false;

            this.DialogResult = DialogResult.OK;
        }

        public void SetStockInfo(ref StockConfig _stockInfo)
        {
            this.StockInfo = _stockInfo;
            txtCode.Text = StockInfo.Code;
            lbName.Text = StockInfo.Name;

            txtCode.Enabled = false;//编辑状态不可更改股票代码,更改股票只能添加和删除
            btnGetStock.Enabled = false;

            btnSave.Enabled = true;

            numPriceCost.Value = StockInfo.PriceCost;
            cbEnable.Checked = StockInfo.Enable;
            if (StockInfo.Rule_PriceAbove.Enable)
            {
                cbPriceAbove.Checked = true;
            }
            numPriceAbove.Value = StockInfo.Rule_PriceAbove.AlertPrice;

            if (StockInfo.Rule_PriceBelow.Enable)
            {
                cbPriceBelow.Checked = true;
            }
            numPriceBelow.Value = StockInfo.Rule_PriceBelow.AlertPrice;

            if (StockInfo.Rule_AmountChange.Enable)
                cbAmountChange.Checked = true;
            if (StockInfo.Rule_PriceChange.Enable)
                cbPriceChange.Checked = true;
            if (StockInfo.Rule_RateAbove.Enable)
            {
                cbRateAbove.Checked = true;
            }
            numRateAbove.Value = StockInfo.Rule_RateAbove.AlertRate;

            if (StockInfo.Rule_RateBelow.Enable)
            {
                cbRateBelow.Checked = true;
            }
            numRateBelow.Value = StockInfo.Rule_RateBelow.AlertRate;

            if (StockInfo.Rule_Peek.Enable)
                cbPeek.Checked = true;
        }

        private void EditStockForm_Load(object sender, EventArgs e)
        {
            
        }
    }
}
