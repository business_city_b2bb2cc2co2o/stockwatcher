﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockWatcher.Model
{
    public class StockPriceInfo
    {
        //股票代码
        public string 股票代码 { get; set; }
        //0：”大秦铁路”，股票名字；
        public string 股票名字 { get; set; }
        //1：”27.55″，今日开盘价；
        public double 今日开盘价 { get; set; }
        //2：”27.25″，昨日收盘价；
        public double 昨日收盘价 { get; set; }
        //3：”26.91″，当前价格；
        public double 当前价格 { get; set; }
        //4：”27.55″，今日最高价；
        public double 今日最高价 { get; set; }
        //5：”26.20″，今日最低价；
        public double 今日最低价 { get; set; }
        //6：”26.91″，竞买价，即“买一”报价；
        public double 竞买价 { get; set; }
        //7：”26.92″，竞卖价，即“卖一”报价；
        public double 竞卖价 { get; set; }
        //8：”22114263″，成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百；
        public long 成交的股票数 { get; set; }
        //9：”589824680″，成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万；
        public double 成交金额 { get; set; }
        //10：”4695″，“买一”申请4695股，即47手；
        public int 买一股数 { get; set; }
        //11：”26.91″，“买一”报价；
        public double 买一价格 { get; set; }
        //12：”57590″，“买二”
        public int 买二股数 { get; set; }
        //13：”26.90″，“买二”
        public double 买二价格 { get; set; }
        //14：”14700″，“买三”
        public int 买三股数 { get; set; }
        //15：”26.89″，“买三”
        public double 买三价格 { get; set; }
        //16：”14300″，“买四”
        public int 买四股数 { get; set; }
        //17：”26.88″，“买四”
        public double 买四价格 { get; set; }
        //18：”15100″，“买五”
        public int 买五股数 { get; set; }
        //19：”26.87″，“买五”
        public double 买五价格 { get; set; }
        //20：”3100″，“卖一”申报3100股，即31手；
        public int 卖一股数 { get; set; }
        //21："36.14" 卖一价格
        public double 卖一价格 { get; set; }
        //22："200" 卖二股数
        public int 卖二股数 { get; set; }
        //23："36.15" 卖二价格
        public double 卖二价格 { get; set; }
        //24："10400" 卖三股数
        public int 卖三股数 { get; set; }
        //25："36.17" 卖三价格
        public double 卖三价格 { get; set; }
        //26："44200" 卖四股数
        public int 卖四股数 { get; set; }
        //27："36.18" 卖四价格
        public double 卖四价格 { get; set; }
        //28："7600" 卖五股数
        public int 卖五股数 { get; set; }
        //29："36.19" 卖五价格
        public double 卖五价格 { get; set; }
        //30："2017/9/11" 日期
        public DateTime 日期 { get; set; }
        //31："11:08:33" 时间
        public DateTime 时间 { get; set; }
        //32："0" 未知
        public double 未知 { get; set; }

        //计算量
        public double 涨幅 { get { return (当前价格 - 昨日收盘价) * 100 / 昨日收盘价; } }
        //设定量
        public double 成本价格 { get; set; }
    }
}
