﻿using StockWatcher.Model.RuleConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model.Rules
{
    public class RuleRateBelow : Rule
    {
        public override RuleTypeEnum Type => RuleTypeEnum.RateBelow;
        public override bool Check(StockPriceInfo stock, StockStatistic statistic)
        {
            if (stock.涨幅 < Convert.ToDouble(Config.AlertRate))
            {
                this.AlertMessage = string.Format("涨幅{0:f1}%<{1:f1}%", stock.涨幅, Config.AlertRate);
                return true;
            }
            else
            {
                this.AlertMessage = "";
                return false;
            }
        }
        public override string AlertLogoChar => "跌";
        public ConfigRate Config { get; set; }

    }
}
