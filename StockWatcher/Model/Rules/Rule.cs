﻿using StockWatcher.Model.RuleConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model.Rules
{
    public abstract class Rule
    {
        public abstract bool Check(StockPriceInfo stock, StockStatistic statistic);
        public abstract string AlertLogoChar { get; }
        public string AlertMessage { get; protected set; }
        public abstract RuleTypeEnum Type { get;  }
    }
}
