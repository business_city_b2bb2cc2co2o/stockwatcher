﻿using StockWatcher.Model.RuleConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model.Rules
{
    public class RulePriceBelow : Rule
    {
        public override RuleTypeEnum Type => RuleTypeEnum.PriceBelow;
        public override bool Check(StockPriceInfo stock, StockStatistic statistic)
        {
            if (stock.当前价格 < Convert.ToDouble(Config.AlertPrice))
            {
                this.AlertMessage = string.Format("现价{0}<{1}", stock.当前价格, Config.AlertPrice);
                return true;
            }
            else
            {
                this.AlertMessage = "";
                return false;
            }
        }
        public override string AlertLogoChar => "低";
        public ConfigPrice Config { get; set; }
             
    }
}
