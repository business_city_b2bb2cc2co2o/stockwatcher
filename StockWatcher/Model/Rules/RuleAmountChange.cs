﻿using StockWatcher.Model.RuleConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model.Rules
{
    public class RuleAmountChange : Rule
    {
        public override RuleTypeEnum Type { get => RuleTypeEnum.AmountChange; }
        public override bool Check(StockPriceInfo stock, StockStatistic statistic)
        {
            if (String.IsNullOrEmpty(statistic.RealCode))//第一次执行,还没有初始化
            {
                this.AlertMessage = "";
                return false;
            }
            double increaseRate = (double)(stock.成交的股票数 - statistic.LastAmount) / (double)statistic.LastAmount;
            TimeSpan timeSpan = stock.时间 - statistic.Time;
            //合理的增长率:每秒均匀增长
            double reasonableRate = timeSpan.TotalSeconds / (4 * 60 * 60);
            //如果增长率大于均匀增长率的5倍.认为成交量突然放大
            if (increaseRate > reasonableRate * 5)
            {
                this.AlertMessage = string.Format("成交量突放,增长率{0:f4}‰/秒", increaseRate * 1000/ timeSpan.TotalSeconds);
                return true;
            }
            else
            {
                this.AlertMessage = "";
                return false;
            }
        }
        public RuleConfig Config { get; set; }
        public override string AlertLogoChar => "量";
    }
}
