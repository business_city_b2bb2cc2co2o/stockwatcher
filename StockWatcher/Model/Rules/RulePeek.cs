﻿using StockWatcher.Model.RuleConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model.Rules
{
    public class RulePeek : Rule
    {
        public override RuleTypeEnum Type { get => RuleTypeEnum.Peek; }

        public override bool Check(StockPriceInfo stock, StockStatistic statistic)
        {
            if (String.IsNullOrEmpty(statistic.RealCode))//第一次执行,还没有初始化
            {
                this.AlertMessage = "";
                return false;
            }
            if (stock.当前价格 >= statistic.HighestPrice)
            {
                this.AlertMessage = string.Format("价格到达新高{0:F2}", stock.当前价格);
                return true;
            }
            else if (stock.当前价格 <= statistic.LowestPrice)
            {
                this.AlertMessage = string.Format("价格到达新低{0:F2}", stock.当前价格);
                return true;
            }
            else
            {
                this.AlertMessage = "";
                return false;
            }
        }
        public RuleConfig Config { get; set; }
        public override string AlertLogoChar => "极";
    }
}
