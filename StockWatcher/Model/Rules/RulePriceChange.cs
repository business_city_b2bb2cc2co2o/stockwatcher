﻿using StockWatcher.Model.RuleConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model.Rules
{
    public class RulePriceChange : Rule
    {
        public override RuleTypeEnum Type => RuleTypeEnum.PriceChange;
        public override bool Check(StockPriceInfo stock, StockStatistic statistic)
        {
            var l3minPrice = statistic.LastNMinPrice(3);
            if (String.IsNullOrEmpty(statistic.RealCode) || l3minPrice == null)//第一次执行,还没有初始化 || 开机时间不足3分钟
            {
                this.AlertMessage = "";
                return false;
            }
            double priceRateOld = ((l3minPrice.price - stock.昨日收盘价) / stock.昨日收盘价) * 100;
            double priceRate = stock.涨幅;

            double increase = priceRate - priceRateOld;
            if (increase >= Config.ChangeRate)
            {
                this.AlertMessage = string.Format("价格突{0}至{2:f2},变化幅度{1:f2}%", priceRate > priceRateOld ? "涨" : "跌", increase, stock.当前价格);
                return true;
            }
            else
            {
                this.AlertMessage = "";
                return false;
            }
        }
        public ConfigChange Config { get; set; }
        public override string AlertLogoChar => "价";
    }
}
