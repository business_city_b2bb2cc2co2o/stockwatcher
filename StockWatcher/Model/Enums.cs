﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model
{
    public enum RuleTypeEnum {
        Peek,
        AmountChange,
        PriceAbove,
        PriceBelow,
        PriceChange,
        RateAbove,
        RateBelow
    }
}
