﻿using StockWatcher.Model.RuleConfigs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model
{
    /// <summary>
    /// 股票信息
    /// </summary>
    public class StockConfig
    {
        public StockConfig()
        {
            Rule_PriceAbove = new ConfigPrice();
            Rule_PriceBelow = new ConfigPrice();
            Rule_AmountChange = new RuleConfig();
            Rule_PriceChange = new ConfigChange();
            Rule_RateAbove = new ConfigRate();
            Rule_RateBelow = new ConfigRate();
            Rule_Peek = new RuleConfig();
        }
        /// <summary>
        /// 股票代码,6位数字
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 真代码,需要加前缀,如上海sh600123,深圳sz000123
        /// </summary>
        public string RealCode { get; set; }
        /// <summary>
        /// 股票名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 是否启用,启用之后才会查询股价
        /// </summary>
        public bool Enable { get; set; }
        /// <summary>
        /// 成本价格
        /// </summary>
        public decimal PriceCost { get; set; }
        /// <summary>
        /// 用于显示名字
        /// </summary>
        public string StockFullName { get { return String.Format("({1}){0}\t{2}", Name.Length < 4 ? Name + "  " : Name, Code, Enable ? "√" : ""); } }

        /// <summary>
        /// 报警条件列表
        /// </summary>
        public ConfigPrice Rule_PriceAbove { get; set; }
        public ConfigPrice Rule_PriceBelow { get; set; }
        public RuleConfig Rule_AmountChange { get; set; }
        public ConfigChange Rule_PriceChange { get; set;}
        public ConfigRate Rule_RateAbove { get; set; }
        public ConfigRate Rule_RateBelow { get; set; }
        public RuleConfig Rule_Peek { get; set; }
    }
}
