﻿using StockWatcher.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model
{
    public class AlertResult
    {  
        public string Code { get; set; }
        public RuleTypeEnum RuleType { get; set; }
        public string AlertLogoChar { get; set; }
        public string Name { get; set; }
        public string AlertMessage { get; set; }
        public DateTime Time { get; set; }
    }

    public class AlertResultForList{
        public string AlertLogos { get; set; }
        public List<string> AlertMessages { get; set; }
    }
}
