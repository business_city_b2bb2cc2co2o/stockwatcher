﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model.RuleConfigs
{
    public class ConfigPrice : RuleConfig
    {
        public decimal AlertPrice { get; set; }
    }
}
