﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model.RuleConfigs
{
    /// <summary>
    /// 各个报警类的父类
    /// </summary>
    public class RuleConfig
    {
        public bool Enable { get; set; }
    }
}
