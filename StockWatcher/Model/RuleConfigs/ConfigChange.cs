﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Model.RuleConfigs
{
    public class ConfigChange : RuleConfig
    {
        public decimal ChangeRatePercent { get; set; }
        public double ChangeRate { get { return (double)ChangeRatePercent; } }

    }
}
