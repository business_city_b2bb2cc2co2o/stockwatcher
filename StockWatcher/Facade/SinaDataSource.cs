﻿using StockWatcher.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace StockWatcher.Facade
{
    class SinaDataSource
    {
        public static string ReadRawData(List<string> stockCodes)
        {
            string codeString = string.Join(",", stockCodes);
            System.Net.WebClient webClient = new System.Net.WebClient();
            webClient.Headers.Add("User-Agent", @"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0");
            webClient.Headers.Add("Accept", @"text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8");
            webClient.Headers.Add("Accept-Language", @"zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
            //webClient.Headers.Add("Accept-Encoding", @"gzip, deflate");
            string url = SystemVar.PRICE_URL + codeString;
            string returnStr = webClient.DownloadString(url);
            return returnStr;
        }

        public static List<StockPriceInfo> ReadData(List<string> stockCodes) {
            string returnData = ReadRawData(stockCodes);
            List<StockPriceInfo> listData = PriceDecode.Decode(returnData);
            return listData;
        }

        public static Image GetStockImage(string stockCode)
        {
            System.Net.WebClient webClient = new System.Net.WebClient();
            webClient.Headers.Add("User-Agent", @"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0");
            webClient.Headers.Add("Accept", @"text/html,application/xhtml+xm…plication/xml;q=0.9,*/*;q=0.8");
            webClient.Headers.Add("Accept-Language", @"zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3");
            string url = string.Format(SystemVar.PRICE_IMG_URL, stockCode);
            Byte[] returnStr = webClient.DownloadData(url);
            Image img = ImageHelper.BytesToImage(returnStr);
            return img;
        }
    }
}
