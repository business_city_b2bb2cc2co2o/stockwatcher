﻿using StockWatcher.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StockWatcher.Facade
{
    public class PriceDecode
    {
        public static Regex reg = new Regex(@"hq_str_(\w{2}\d{6})=\""(.*)\"";");

        public static List<StockPriceInfo> Decode(string priceData)
        {
            MatchCollection matches = reg.Matches(priceData);
            List<StockPriceInfo> ret = new List<StockPriceInfo>();
            foreach (Match m in matches)
            {
                StockPriceInfo p = new StockPriceInfo();
                p.股票代码 = m.Groups[1].Value;
                if (string.IsNullOrEmpty(m.Groups[2].Value))
                {
                    ret.Add(p);
                    continue;
                }
                try
                {
                    string[] codes = m.Groups[2].Value.Split(',');
                    p.股票名字 = codes[0];
                    p.今日开盘价 = double.Parse(codes[1]);
                    p.昨日收盘价 = double.Parse(codes[2]);
                    p.当前价格 = double.Parse(codes[3]);
                    p.今日最高价 = double.Parse(codes[4]);
                    p.今日最低价 = double.Parse(codes[5]);
                    p.竞买价 = double.Parse(codes[6]);
                    p.竞卖价 = double.Parse(codes[7]);
                    p.成交的股票数 = long.Parse(codes[8]);
                    p.成交金额 = double.Parse(codes[9]);
                    p.买一股数 = int.Parse(codes[10]);
                    p.买一价格 = double.Parse(codes[11]);
                    p.买二股数 = int.Parse(codes[12]);
                    p.买二价格 = double.Parse(codes[13]);
                    p.买三股数 = int.Parse(codes[14]);
                    p.买三价格 = double.Parse(codes[15]);
                    p.买四股数 = int.Parse(codes[16]);
                    p.买四价格 = double.Parse(codes[17]);
                    p.买五股数 = int.Parse(codes[18]);
                    p.买五价格 = double.Parse(codes[19]);
                    p.卖一股数 = int.Parse(codes[20]);
                    p.卖一价格 = double.Parse(codes[21]);
                    p.卖二股数 = int.Parse(codes[22]);
                    p.卖二价格 = double.Parse(codes[23]);
                    p.卖三股数 = int.Parse(codes[24]);
                    p.卖三价格 = double.Parse(codes[25]);
                    p.卖四股数 = int.Parse(codes[26]);
                    p.卖四价格 = double.Parse(codes[27]);
                    p.卖五股数 = int.Parse(codes[28]);
                    p.卖五价格 = double.Parse(codes[29]);
                    p.日期 = DateTime.Parse(codes[30] + " " + codes[31]);
                    p.时间 = p.日期;
                    p.未知 = double.Parse(codes[32]);

                    p.成本价格 = Convert.ToDouble(SystemVar.StockConfigs[p.股票代码].PriceCost);
                }
                catch
                { }
                finally
                {
                    ret.Add(p);
                }
            }
            return ret;
        }
    }
}
