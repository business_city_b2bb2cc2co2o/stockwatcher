﻿using StockWatcher.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
namespace StockWatcher.Facade
{
    class ConfigHelper
    {
        const string CONFIG_FILE_PATH = "stock_config.xml";

        public static void SaveStockConfig()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<StockConfig>));
            using (TextWriter tw = new StreamWriter(CONFIG_FILE_PATH))
            {
                serializer.Serialize(tw, SystemVar.StockConfigs.Values.ToList());
            }
        }
        public static void LoadStockConfig()
        {
            Dictionary<string, StockConfig> config = new Dictionary<string, StockConfig>();
            if (File.Exists(CONFIG_FILE_PATH))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<StockConfig>));
                using (TextReader tr = new StreamReader(CONFIG_FILE_PATH))
                {
                    try
                    {
                        List<StockConfig> tempConfig = (List<StockConfig>)serializer.Deserialize(tr);
                        tempConfig.ForEach(a => config.Add(a.RealCode, a));
                    }
                    catch
                    {
                    }
                }
            }
            SystemVar.StockConfigs = config;
        }
    }
}
