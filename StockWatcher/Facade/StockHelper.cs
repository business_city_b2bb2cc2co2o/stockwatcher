﻿using StockWatcher.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher.Facade
{
    public class StockHelper
    {
        public static string GetRealStockCode(string stockCode) {
            if (stockCode.Length != 6)
                throw new Exception("股票代码不是6位");
            if (!int.TryParse(stockCode, out int stockCodeInt))
                throw new Exception("必须输入数字");
            string codePrefix = "";
            switch (stockCode[0]) {
                case '6':
                    codePrefix = "sh";
                    break;
                case '3':
                case '0':
                    codePrefix = "sz";
                    break;
            }
            if(codePrefix=="")
                throw new Exception("无法判断");
            return codePrefix + stockCode;
        }
        public static Dictionary<string, StockConfig> GetEnabledStock(Dictionary<string, StockConfig> allStock)
        {
            List<StockConfig> stocks = allStock.Values.ToList();
            stocks = stocks.FindAll(a => a.Enable);
            Dictionary<string, StockConfig> enabledStock = new Dictionary<string, StockConfig>();
            stocks.ForEach(a => enabledStock.Add(a.RealCode, a));
            return enabledStock;
        }
        public static string GetStockDetailString(StockPriceInfo realTimePrice) {
            if (realTimePrice == null)
                return "";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(String.Format("时间:\t\t{0}", realTimePrice.日期.ToLongTimeString()));
            sb.AppendLine(String.Format("股票:\t\t({0}){1}", realTimePrice.股票代码, realTimePrice.股票名字));
            sb.AppendLine(String.Format("当前价格:\t{0}({1:f2}%)", realTimePrice.当前价格, realTimePrice.涨幅));
            sb.AppendLine("------------------");
            sb.AppendLine(String.Format("最高-最低:\t{0}-{1}", realTimePrice.今日最高价,realTimePrice.今日最低价));
            sb.AppendLine(String.Format("成交的股票数:\t{0}", realTimePrice.成交的股票数.ToString("0,00")));
            sb.AppendLine(String.Format("成交金额:\t{0}", realTimePrice.成交金额.ToString("0,00")));
            sb.AppendLine(String.Format("今日开盘价:\t{0}", realTimePrice.今日开盘价));
            sb.AppendLine(String.Format("昨日收盘价:\t{0}", realTimePrice.昨日收盘价));
            sb.AppendLine("------------------");
            sb.AppendLine(String.Format("竞买-竞卖:\t{0}-{1}", realTimePrice.竞买价,realTimePrice.竞卖价));
            sb.AppendLine();
            sb.AppendLine(String.Format("卖五:\t\t{0}\t({1}手)", realTimePrice.卖五价格, realTimePrice.卖五股数/100));
            sb.AppendLine(String.Format("卖四:\t\t{0}\t({1}手)", realTimePrice.卖四价格, realTimePrice.卖四股数/100));
            sb.AppendLine(String.Format("卖三:\t\t{0}\t({1}手)", realTimePrice.卖三价格, realTimePrice.卖三股数/100));
            sb.AppendLine(String.Format("卖二:\t\t{0}\t({1}手)", realTimePrice.卖二价格, realTimePrice.卖二股数/100));
            sb.AppendLine(String.Format("卖一:\t\t{0}\t({1}手)", realTimePrice.卖一价格, realTimePrice.卖一股数/100));
            sb.AppendLine();
            sb.AppendLine(String.Format("买一:\t\t{0}\t({1}手)", realTimePrice.买一价格, realTimePrice.买一股数/100));
            sb.AppendLine(String.Format("买二:\t\t{0}\t({1}手)", realTimePrice.买二价格, realTimePrice.买二股数/100));
            sb.AppendLine(String.Format("买三:\t\t{0}\t({1}手)", realTimePrice.买三价格, realTimePrice.买三股数/100));
            sb.AppendLine(String.Format("买四:\t\t{0}\t({1}手)", realTimePrice.买四价格, realTimePrice.买四股数/100));
            sb.AppendLine(String.Format("买五:\t\t{0}\t({1}手)", realTimePrice.买五价格, realTimePrice.买五股数/100));

            return sb.ToString();
        }
    }
}
