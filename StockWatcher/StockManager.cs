﻿using StockWatcher.Model;
using StockWatcher.Model.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockWatcher
{
    public class StockManager
    {
        public Dictionary<String, StockConfig> StockConfigs;

        public Dictionary<String, StockStatistic> StockStatistics;

        public Dictionary<String, List<Rule>> StockRules;

        public StockManager(Dictionary<String, StockConfig> _stockConfigs)
        {
            this.StockConfigs = new Dictionary<string, StockConfig>();
            StockStatistics = new Dictionary<string, Model.StockStatistic>();
            StockRules = new Dictionary<string, List<Rule>>();
            foreach (var code in _stockConfigs.Keys)
            {
                AddStock(_stockConfigs[code]);
            }
        }
        public void RemoveStock(string code)
        {
            StockConfigs.Remove(code);
            StockStatistics.Remove(code);
            StockRules.Remove(code);
        }
        /// <summary>
        /// 编辑,保留刚才的统计数据
        /// </summary>
        /// <param name="tempConfig"></param>
        public void EditStock(StockConfig tempConfig)
        {
            if (tempConfig.Enable)
            {
                string code = tempConfig.RealCode;
                this.StockConfigs[code] = tempConfig;
                UpdateRuleList(code);
                if (!StockStatistics.ContainsKey(code))
                    StockStatistics.Add(code, new StockStatistic());
            }
            else
            {
                RemoveStock(tempConfig.RealCode);
            }
        }

        public void AddStock(StockConfig tempConfig) {
            if (tempConfig.Enable)
            {
                string code = tempConfig.RealCode;
                this.StockConfigs.Add(code, tempConfig);

                StockStatistics.Add(code, new StockStatistic());

                UpdateRuleList(code);
            }
        }

        private void UpdateRuleList(string code)
        {
            List<Rule> ruleList = new List<Rule>();
            if (this.StockConfigs[code].Rule_PriceAbove.Enable)
                ruleList.Add(new RulePriceAbove() { Config = this.StockConfigs[code].Rule_PriceAbove });
            if (this.StockConfigs[code].Rule_PriceBelow.Enable)
                ruleList.Add(new RulePriceBelow() { Config = this.StockConfigs[code].Rule_PriceBelow });

            if (this.StockConfigs[code].Rule_RateBelow.Enable)
                ruleList.Add(new RuleRateBelow() { Config = this.StockConfigs[code].Rule_RateBelow });
            if (this.StockConfigs[code].Rule_RateAbove.Enable)
                ruleList.Add(new RuleRateAbove() { Config = this.StockConfigs[code].Rule_RateAbove });

            if (this.StockConfigs[code].Rule_Peek.Enable)
                ruleList.Add(new RulePeek() { Config = this.StockConfigs[code].Rule_Peek });
            if (this.StockConfigs[code].Rule_AmountChange.Enable)
                ruleList.Add(new RuleAmountChange() { Config = this.StockConfigs[code].Rule_AmountChange });
            if (this.StockConfigs[code].Rule_PriceChange.Enable)
                ruleList.Add(new RulePriceChange() { Config = this.StockConfigs[code].Rule_PriceChange });
            if (StockRules.ContainsKey(code))
                StockRules.Remove(code);
            StockRules.Add(code, ruleList);
        }

        public Dictionary<string, List<AlertResult>> CheckData(List<StockPriceInfo> stockPrices)
        {
            Dictionary<string, List<AlertResult>> allStockAlertResult = new Dictionary<string, List<AlertResult>>();
            stockPrices.ForEach(s =>
            {
                List<AlertResult> oneStockAlertResult = new List<AlertResult>();
                //check alert
                foreach (var rule in StockRules[s.股票代码])
                {
                    if (rule.Check(s, StockStatistics[s.股票代码]))
                    {
                        AlertResult alert = new AlertResult();
                        alert.Name = s.股票名字;
                        alert.Code = s.股票代码;
                        alert.RuleType = rule.Type;
                        alert.AlertMessage=rule.AlertMessage;
                        alert.AlertLogoChar = rule.AlertLogoChar;
                        alert.Time = s.时间;
                        oneStockAlertResult.Add(alert);
                    }
                }
                allStockAlertResult.Add(s.股票代码, oneStockAlertResult);
                //update statistic
                StockStatistics[s.股票代码].Update(s);
            });

            return allStockAlertResult;
        }

    }
   


}
