﻿namespace StockWatcher
{
    partial class FrmNote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtMessageArea = new System.Windows.Forms.RichTextBox();
            this.tmrClose = new System.Windows.Forms.Timer(this.components);
            this.tmrCountDown = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // txtMessageArea
            // 
            this.txtMessageArea.BackColor = System.Drawing.Color.Black;
            this.txtMessageArea.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMessageArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMessageArea.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtMessageArea.ForeColor = System.Drawing.Color.White;
            this.txtMessageArea.Location = new System.Drawing.Point(0, 0);
            this.txtMessageArea.Margin = new System.Windows.Forms.Padding(15);
            this.txtMessageArea.Name = "txtMessageArea";
            this.txtMessageArea.Size = new System.Drawing.Size(415, 150);
            this.txtMessageArea.TabIndex = 0;
            this.txtMessageArea.Text = "";
            // 
            // tmrClose
            // 
            this.tmrClose.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tmrCountDown
            // 
            this.tmrCountDown.Interval = 1000;
            this.tmrCountDown.Tick += new System.EventHandler(this.tmrCountDown_Tick);
            // 
            // FrmNote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 150);
            this.Controls.Add(this.txtMessageArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmNote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmNote";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmNote_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtMessageArea;
        private System.Windows.Forms.Timer tmrClose;
        private System.Windows.Forms.Timer tmrCountDown;
    }
}