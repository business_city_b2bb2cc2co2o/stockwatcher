﻿namespace StockWatcher
{
    partial class StockWatcherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StockWatcherForm));
            this.groupBox1 = new System.Windows.Forms.Panel();
            this.groupStockAdmin = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.listStocks = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnStockEdit = new System.Windows.Forms.Button();
            this.btnStockAdd = new System.Windows.Forms.Button();
            this.btnStockDelete = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.pbSecCount = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.nudTimeSpan = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lvStockList = new System.Windows.Forms.ListView();
            this.chCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAlert = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAlartPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chRate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAmount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chHigh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chLow = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel4 = new System.Windows.Forms.Panel();
            this.lbMessage = new System.Windows.Forms.Label();
            this.picStockPrice = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.GroupBox();
            this.txtAlert = new System.Windows.Forms.TextBox();
            this.txtStockDetail = new System.Windows.Forms.TextBox();
            this.tmrWorker = new System.Windows.Forms.Timer(this.components);
            this.tmrProcessbar = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupStockAdmin.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeSpan)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStockPrice)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupStockAdmin);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(192, 545);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.Text = "设置";
            // 
            // groupStockAdmin
            // 
            this.groupStockAdmin.Controls.Add(this.panel3);
            this.groupStockAdmin.Controls.Add(this.panel2);
            this.groupStockAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupStockAdmin.Location = new System.Drawing.Point(0, 120);
            this.groupStockAdmin.Name = "groupStockAdmin";
            this.groupStockAdmin.Size = new System.Drawing.Size(192, 425);
            this.groupStockAdmin.TabIndex = 1;
            this.groupStockAdmin.TabStop = false;
            this.groupStockAdmin.Text = "股票列表";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.listStocks);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 60);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(186, 362);
            this.panel3.TabIndex = 1;
            // 
            // listStocks
            // 
            this.listStocks.DisplayMember = "StockFullName";
            this.listStocks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listStocks.FormattingEnabled = true;
            this.listStocks.ItemHeight = 12;
            this.listStocks.Location = new System.Drawing.Point(0, 0);
            this.listStocks.Name = "listStocks";
            this.listStocks.Size = new System.Drawing.Size(186, 362);
            this.listStocks.TabIndex = 0;
            this.listStocks.ValueMember = "RealCode";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnStockEdit);
            this.panel2.Controls.Add(this.btnStockAdd);
            this.panel2.Controls.Add(this.btnStockDelete);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(186, 43);
            this.panel2.TabIndex = 0;
            // 
            // btnStockEdit
            // 
            this.btnStockEdit.BackColor = System.Drawing.SystemColors.Control;
            this.btnStockEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockEdit.ForeColor = System.Drawing.Color.Black;
            this.btnStockEdit.Location = new System.Drawing.Point(52, 7);
            this.btnStockEdit.Name = "btnStockEdit";
            this.btnStockEdit.Size = new System.Drawing.Size(45, 30);
            this.btnStockEdit.TabIndex = 16;
            this.btnStockEdit.Text = "编辑";
            this.btnStockEdit.UseVisualStyleBackColor = false;
            this.btnStockEdit.Click += new System.EventHandler(this.btnStockEdit_Click);
            // 
            // btnStockAdd
            // 
            this.btnStockAdd.BackColor = System.Drawing.SystemColors.Control;
            this.btnStockAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockAdd.ForeColor = System.Drawing.Color.Black;
            this.btnStockAdd.Location = new System.Drawing.Point(6, 7);
            this.btnStockAdd.Name = "btnStockAdd";
            this.btnStockAdd.Size = new System.Drawing.Size(45, 30);
            this.btnStockAdd.TabIndex = 14;
            this.btnStockAdd.Text = "添加";
            this.btnStockAdd.UseVisualStyleBackColor = false;
            this.btnStockAdd.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnStockDelete
            // 
            this.btnStockDelete.BackColor = System.Drawing.SystemColors.Control;
            this.btnStockDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockDelete.ForeColor = System.Drawing.Color.Black;
            this.btnStockDelete.Location = new System.Drawing.Point(98, 7);
            this.btnStockDelete.Name = "btnStockDelete";
            this.btnStockDelete.Size = new System.Drawing.Size(45, 30);
            this.btnStockDelete.TabIndex = 15;
            this.btnStockDelete.Text = "删除";
            this.btnStockDelete.UseVisualStyleBackColor = false;
            this.btnStockDelete.Click += new System.EventHandler(this.btnStockDelete_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnRefresh);
            this.groupBox3.Controls.Add(this.btnStart);
            this.groupBox3.Controls.Add(this.btnStop);
            this.groupBox3.Controls.Add(this.pbSecCount);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.nudTimeSpan);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(192, 120);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "刷新控制";
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.ForeColor = System.Drawing.Color.Navy;
            this.btnRefresh.Location = new System.Drawing.Point(103, 80);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(45, 30);
            this.btnRefresh.TabIndex = 13;
            this.btnRefresh.Text = "刷新";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.PaleGreen;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Location = new System.Drawing.Point(11, 80);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(45, 30);
            this.btnStart.TabIndex = 11;
            this.btnStart.Text = "开始";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.Salmon;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Location = new System.Drawing.Point(57, 80);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(45, 30);
            this.btnStop.TabIndex = 12;
            this.btnStop.Text = "停止";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // pbSecCount
            // 
            this.pbSecCount.Location = new System.Drawing.Point(11, 51);
            this.pbSecCount.Name = "pbSecCount";
            this.pbSecCount.Size = new System.Drawing.Size(158, 23);
            this.pbSecCount.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 9;
            this.label2.Text = "刷新间隔(秒)";
            // 
            // nudTimeSpan
            // 
            this.nudTimeSpan.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudTimeSpan.Location = new System.Drawing.Point(110, 24);
            this.nudTimeSpan.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudTimeSpan.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudTimeSpan.Name = "nudTimeSpan";
            this.nudTimeSpan.Size = new System.Drawing.Size(57, 21);
            this.nudTimeSpan.TabIndex = 8;
            this.nudTimeSpan.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.splitContainer1);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(192, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(910, 545);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "数据查看";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(3, 17);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lvStockList);
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.picStockPrice);
            this.splitContainer1.Size = new System.Drawing.Size(632, 525);
            this.splitContainer1.SplitterDistance = 219;
            this.splitContainer1.TabIndex = 1;
            // 
            // lvStockList
            // 
            this.lvStockList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chCode,
            this.chName,
            this.chAlert,
            this.chPrice,
            this.chAlartPrice,
            this.chRate,
            this.chAmount,
            this.chHigh,
            this.chLow});
            this.lvStockList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvStockList.Location = new System.Drawing.Point(0, 33);
            this.lvStockList.Name = "lvStockList";
            this.lvStockList.Size = new System.Drawing.Size(632, 186);
            this.lvStockList.TabIndex = 3;
            this.lvStockList.UseCompatibleStateImageBehavior = false;
            this.lvStockList.View = System.Windows.Forms.View.Details;
            this.lvStockList.SelectedIndexChanged += new System.EventHandler(this.lvStockList_SelectedIndexChanged);
            // 
            // chCode
            // 
            this.chCode.Text = "代码";
            this.chCode.Width = 70;
            // 
            // chName
            // 
            this.chName.Text = "名称";
            this.chName.Width = 100;
            // 
            // chAlert
            // 
            this.chAlert.Text = "报警";
            this.chAlert.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chAlert.Width = 70;
            // 
            // chPrice
            // 
            this.chPrice.Text = "价格";
            this.chPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chPrice.Width = 70;
            // 
            // chAlartPrice
            // 
            this.chAlartPrice.Text = "成本/幅度";
            this.chAlartPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chAlartPrice.Width = 110;
            // 
            // chRate
            // 
            this.chRate.Text = "涨幅";
            this.chRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chAmount
            // 
            this.chAmount.Text = "成交量";
            this.chAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chAmount.Width = 90;
            // 
            // chHigh
            // 
            this.chHigh.Text = "最高";
            this.chHigh.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chHigh.Width = 70;
            // 
            // chLow
            // 
            this.chLow.Text = "最低";
            this.chLow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chLow.Width = 70;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lbMessage);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(632, 33);
            this.panel4.TabIndex = 2;
            // 
            // lbMessage
            // 
            this.lbMessage.AutoSize = true;
            this.lbMessage.ForeColor = System.Drawing.Color.Tomato;
            this.lbMessage.Location = new System.Drawing.Point(3, 9);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(71, 12);
            this.lbMessage.TabIndex = 0;
            this.lbMessage.Text = "祝君发大财.";
            this.lbMessage.Click += new System.EventHandler(this.lbMessage_Click);
            // 
            // picStockPrice
            // 
            this.picStockPrice.BackColor = System.Drawing.Color.White;
            this.picStockPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picStockPrice.Location = new System.Drawing.Point(0, 0);
            this.picStockPrice.Name = "picStockPrice";
            this.picStockPrice.Size = new System.Drawing.Size(632, 302);
            this.picStockPrice.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picStockPrice.TabIndex = 3;
            this.picStockPrice.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtAlert);
            this.panel1.Controls.Add(this.txtStockDetail);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(635, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(272, 525);
            this.panel1.TabIndex = 0;
            this.panel1.TabStop = false;
            this.panel1.Text = "详细信息";
            // 
            // txtAlert
            // 
            this.txtAlert.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAlert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAlert.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtAlert.ForeColor = System.Drawing.Color.Tomato;
            this.txtAlert.Location = new System.Drawing.Point(3, 372);
            this.txtAlert.Multiline = true;
            this.txtAlert.Name = "txtAlert";
            this.txtAlert.Size = new System.Drawing.Size(266, 150);
            this.txtAlert.TabIndex = 1;
            // 
            // txtStockDetail
            // 
            this.txtStockDetail.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtStockDetail.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtStockDetail.Location = new System.Drawing.Point(3, 17);
            this.txtStockDetail.Multiline = true;
            this.txtStockDetail.Name = "txtStockDetail";
            this.txtStockDetail.Size = new System.Drawing.Size(266, 355);
            this.txtStockDetail.TabIndex = 0;
            // 
            // tmrWorker
            // 
            this.tmrWorker.Tick += new System.EventHandler(this.tmrWorker_Tick);
            // 
            // tmrProcessbar
            // 
            this.tmrProcessbar.Interval = 1000;
            this.tmrProcessbar.Tick += new System.EventHandler(this.tmrProcessbar_Tick);
            // 
            // StockWatcherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1102, 545);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StockWatcherForm";
            this.Text = "股票监视器";
            this.Load += new System.EventHandler(this.StockWatcherForm_Load);
            this.SizeChanged += new System.EventHandler(this.StockWatcherForm_SizeChanged);
            this.groupBox1.ResumeLayout(false);
            this.groupStockAdmin.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTimeSpan)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStockPrice)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel groupBox1;
        private System.Windows.Forms.GroupBox groupStockAdmin;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox panel1;
        private System.Windows.Forms.ProgressBar pbSecCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudTimeSpan;
        private System.Windows.Forms.ListBox listStocks;
        private System.Windows.Forms.Button btnStockEdit;
        private System.Windows.Forms.Button btnStockAdd;
        private System.Windows.Forms.Button btnStockDelete;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.ListView lvStockList;
        private System.Windows.Forms.ColumnHeader chCode;
        private System.Windows.Forms.ColumnHeader chName;
        private System.Windows.Forms.ColumnHeader chPrice;
        private System.Windows.Forms.ColumnHeader chAlartPrice;
        private System.Windows.Forms.ColumnHeader chRate;
        private System.Windows.Forms.ColumnHeader chAmount;
        private System.Windows.Forms.ColumnHeader chHigh;
        private System.Windows.Forms.ColumnHeader chLow;
        private System.Windows.Forms.PictureBox picStockPrice;
        private System.Windows.Forms.TextBox txtStockDetail;
        private System.Windows.Forms.ColumnHeader chAlert;
        private System.Windows.Forms.TextBox txtAlert;
        private System.Windows.Forms.Timer tmrWorker;
        private System.Windows.Forms.Timer tmrProcessbar;
    }
}