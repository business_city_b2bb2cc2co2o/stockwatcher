﻿namespace StockWatcher
{
    partial class EditStockForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.cbPriceAbove = new System.Windows.Forms.CheckBox();
            this.cbAmountChange = new System.Windows.Forms.CheckBox();
            this.cbPriceChange = new System.Windows.Forms.CheckBox();
            this.cbRateAbove = new System.Windows.Forms.CheckBox();
            this.cbRateBelow = new System.Windows.Forms.CheckBox();
            this.cbPriceBelow = new System.Windows.Forms.CheckBox();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.btnGetStock = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cbPeek = new System.Windows.Forms.CheckBox();
            this.numPriceAbove = new System.Windows.Forms.NumericUpDown();
            this.numPriceBelow = new System.Windows.Forms.NumericUpDown();
            this.numRateAbove = new System.Windows.Forms.NumericUpDown();
            this.numRateBelow = new System.Windows.Forms.NumericUpDown();
            this.numPriceCost = new System.Windows.Forms.NumericUpDown();
            this.cbEnable = new System.Windows.Forms.CheckBox();
            this.numPriceChange = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceAbove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceBelow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRateAbove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRateBelow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceChange)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "股票代码";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "股票名称";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(115, 60);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(29, 12);
            this.lbName.TabIndex = 3;
            this.lbName.Text = "????";
            // 
            // cbPriceAbove
            // 
            this.cbPriceAbove.AutoSize = true;
            this.cbPriceAbove.Location = new System.Drawing.Point(17, 163);
            this.cbPriceAbove.Name = "cbPriceAbove";
            this.cbPriceAbove.Size = new System.Drawing.Size(72, 16);
            this.cbPriceAbove.TabIndex = 4;
            this.cbPriceAbove.Text = "股价大于";
            this.cbPriceAbove.UseVisualStyleBackColor = true;
            // 
            // cbAmountChange
            // 
            this.cbAmountChange.AutoSize = true;
            this.cbAmountChange.Location = new System.Drawing.Point(17, 239);
            this.cbAmountChange.Name = "cbAmountChange";
            this.cbAmountChange.Size = new System.Drawing.Size(84, 16);
            this.cbAmountChange.TabIndex = 5;
            this.cbAmountChange.Text = "交易量变大";
            this.cbAmountChange.UseVisualStyleBackColor = true;
            // 
            // cbPriceChange
            // 
            this.cbPriceChange.AutoSize = true;
            this.cbPriceChange.Location = new System.Drawing.Point(17, 277);
            this.cbPriceChange.Name = "cbPriceChange";
            this.cbPriceChange.Size = new System.Drawing.Size(96, 16);
            this.cbPriceChange.TabIndex = 6;
            this.cbPriceChange.Text = "3min涨幅变化";
            this.cbPriceChange.UseVisualStyleBackColor = true;
            // 
            // cbRateAbove
            // 
            this.cbRateAbove.AutoSize = true;
            this.cbRateAbove.Location = new System.Drawing.Point(17, 315);
            this.cbRateAbove.Name = "cbRateAbove";
            this.cbRateAbove.Size = new System.Drawing.Size(72, 16);
            this.cbRateAbove.TabIndex = 7;
            this.cbRateAbove.Text = "涨幅大于";
            this.cbRateAbove.UseVisualStyleBackColor = true;
            // 
            // cbRateBelow
            // 
            this.cbRateBelow.AutoSize = true;
            this.cbRateBelow.Location = new System.Drawing.Point(17, 353);
            this.cbRateBelow.Name = "cbRateBelow";
            this.cbRateBelow.Size = new System.Drawing.Size(72, 16);
            this.cbRateBelow.TabIndex = 8;
            this.cbRateBelow.Text = "涨幅小于";
            this.cbRateBelow.UseVisualStyleBackColor = true;
            // 
            // cbPriceBelow
            // 
            this.cbPriceBelow.AutoSize = true;
            this.cbPriceBelow.Location = new System.Drawing.Point(17, 201);
            this.cbPriceBelow.Name = "cbPriceBelow";
            this.cbPriceBelow.Size = new System.Drawing.Size(72, 16);
            this.cbPriceBelow.TabIndex = 9;
            this.cbPriceBelow.Text = "股价小于";
            this.cbPriceBelow.UseVisualStyleBackColor = true;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(117, 23);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(96, 21);
            this.txtCode.TabIndex = 10;
            // 
            // btnGetStock
            // 
            this.btnGetStock.Location = new System.Drawing.Point(219, 21);
            this.btnGetStock.Name = "btnGetStock";
            this.btnGetStock.Size = new System.Drawing.Size(50, 23);
            this.btnGetStock.TabIndex = 11;
            this.btnGetStock.Text = "查询";
            this.btnGetStock.UseVisualStyleBackColor = true;
            this.btnGetStock.Click += new System.EventHandler(this.btnGetStock_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "成本价";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(98, 431);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(17, 431);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbPeek
            // 
            this.cbPeek.AutoSize = true;
            this.cbPeek.Location = new System.Drawing.Point(17, 386);
            this.cbPeek.Name = "cbPeek";
            this.cbPeek.Size = new System.Drawing.Size(102, 16);
            this.cbPeek.TabIndex = 20;
            this.cbPeek.Text = "到达新高/新低";
            this.cbPeek.UseVisualStyleBackColor = true;
            // 
            // numPriceAbove
            // 
            this.numPriceAbove.DecimalPlaces = 2;
            this.numPriceAbove.Location = new System.Drawing.Point(117, 162);
            this.numPriceAbove.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPriceAbove.Name = "numPriceAbove";
            this.numPriceAbove.Size = new System.Drawing.Size(96, 21);
            this.numPriceAbove.TabIndex = 21;
            this.numPriceAbove.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numPriceBelow
            // 
            this.numPriceBelow.DecimalPlaces = 2;
            this.numPriceBelow.Location = new System.Drawing.Point(117, 200);
            this.numPriceBelow.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPriceBelow.Name = "numPriceBelow";
            this.numPriceBelow.Size = new System.Drawing.Size(96, 21);
            this.numPriceBelow.TabIndex = 22;
            this.numPriceBelow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numRateAbove
            // 
            this.numRateAbove.DecimalPlaces = 2;
            this.numRateAbove.Location = new System.Drawing.Point(117, 314);
            this.numRateAbove.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numRateAbove.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.numRateAbove.Name = "numRateAbove";
            this.numRateAbove.Size = new System.Drawing.Size(96, 21);
            this.numRateAbove.TabIndex = 23;
            this.numRateAbove.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numRateBelow
            // 
            this.numRateBelow.DecimalPlaces = 2;
            this.numRateBelow.Location = new System.Drawing.Point(117, 352);
            this.numRateBelow.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numRateBelow.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.numRateBelow.Name = "numRateBelow";
            this.numRateBelow.Size = new System.Drawing.Size(96, 21);
            this.numRateBelow.TabIndex = 24;
            this.numRateBelow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numPriceCost
            // 
            this.numPriceCost.DecimalPlaces = 2;
            this.numPriceCost.Location = new System.Drawing.Point(117, 92);
            this.numPriceCost.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numPriceCost.Name = "numPriceCost";
            this.numPriceCost.Size = new System.Drawing.Size(96, 21);
            this.numPriceCost.TabIndex = 25;
            this.numPriceCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbEnable
            // 
            this.cbEnable.AutoSize = true;
            this.cbEnable.Checked = true;
            this.cbEnable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbEnable.Location = new System.Drawing.Point(17, 130);
            this.cbEnable.Name = "cbEnable";
            this.cbEnable.Size = new System.Drawing.Size(48, 16);
            this.cbEnable.TabIndex = 26;
            this.cbEnable.Text = "启用";
            this.cbEnable.UseVisualStyleBackColor = true;
            // 
            // numPriceChange
            // 
            this.numPriceChange.DecimalPlaces = 2;
            this.numPriceChange.Location = new System.Drawing.Point(117, 276);
            this.numPriceChange.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPriceChange.Name = "numPriceChange";
            this.numPriceChange.Size = new System.Drawing.Size(96, 21);
            this.numPriceChange.TabIndex = 27;
            this.numPriceChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numPriceChange.Value = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(219, 278);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 28;
            this.label4.Text = "%";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(217, 316);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 12);
            this.label5.TabIndex = 29;
            this.label5.Text = "%";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(217, 354);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 12);
            this.label6.TabIndex = 30;
            this.label6.Text = "%";
            // 
            // EditStockForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(275, 473);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numPriceChange);
            this.Controls.Add(this.cbEnable);
            this.Controls.Add(this.numPriceCost);
            this.Controls.Add(this.numRateBelow);
            this.Controls.Add(this.numRateAbove);
            this.Controls.Add(this.numPriceBelow);
            this.Controls.Add(this.numPriceAbove);
            this.Controls.Add(this.cbPeek);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnGetStock);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.cbPriceBelow);
            this.Controls.Add(this.cbRateBelow);
            this.Controls.Add(this.cbRateAbove);
            this.Controls.Add(this.cbPriceChange);
            this.Controls.Add(this.cbAmountChange);
            this.Controls.Add(this.cbPriceAbove);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EditStockForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "编辑股票";
            this.Load += new System.EventHandler(this.EditStockForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numPriceAbove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceBelow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRateAbove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRateBelow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceChange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.CheckBox cbPriceAbove;
        private System.Windows.Forms.CheckBox cbAmountChange;
        private System.Windows.Forms.CheckBox cbPriceChange;
        private System.Windows.Forms.CheckBox cbRateAbove;
        private System.Windows.Forms.CheckBox cbRateBelow;
        private System.Windows.Forms.CheckBox cbPriceBelow;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Button btnGetStock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.CheckBox cbPeek;
        private System.Windows.Forms.NumericUpDown numPriceAbove;
        private System.Windows.Forms.NumericUpDown numPriceBelow;
        private System.Windows.Forms.NumericUpDown numRateAbove;
        private System.Windows.Forms.NumericUpDown numRateBelow;
        private System.Windows.Forms.NumericUpDown numPriceCost;
        private System.Windows.Forms.CheckBox cbEnable;
        private System.Windows.Forms.NumericUpDown numPriceChange;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}